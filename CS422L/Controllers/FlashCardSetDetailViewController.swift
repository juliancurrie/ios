//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        makeItPretty()
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any)
    {
        let newCard = Flashcard()
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(item: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    @IBAction func studyButton(_ sender: Any) {
    
    func tableView(_ tableView: UITableView, cards: [Flashcard], indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToStudy", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let alert = UIAlertController(title: cards[indexPath.row].term, message: cards[indexPath.row].definition, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler:{ action in
            self.editCard(indexPath: indexPath)}))
        present(alert, animated: true, completion: nil)
    }
    
    func editCard(indexPath: IndexPath)
    {
        let editAlert = UIAlertController(title: cards[indexPath.row].term, message: cards[indexPath.row].definition, preferredStyle: .alert)
        editAlert.addTextField { [self] (textField : UITextField!) -> Void in
            textField.placeholder = self.cards[indexPath.row].term
        }
        editAlert.addTextField { [self] (textField : UITextField!) -> Void in
            textField.placeholder = self.cards[indexPath.row].definition
        }
        editAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: nil))
        editAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: nil))
        present(editAlert, animated: true, completion: nil)
    }

    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    

    func studyCards()
    {
        
    }
    
}
