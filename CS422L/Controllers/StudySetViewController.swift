//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Julian Currie on 2/14/22.
//

import UIKit

class StudySetViewController: UIViewController
{
//    var cards: [Flashcard] = [Flashcard]()
//
//    init(flashcards: FlashCardSetDetailViewController)
//    {
//        self.cards = flashcards.getCards()
//        super.init(nibName: nil, bundle: nil)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//            super.init(coder: aDecoder)
//    }
    
    @IBOutlet weak var topCardView: UIView!
    @IBOutlet weak var midCardView: UIView!
    @IBOutlet weak var botCardView: UIView!
    @IBOutlet weak var termView: UIView!
    @IBOutlet weak var defintionView: UIView!
    
    @IBOutlet weak var termLabel: UILabel!
    @IBOutlet weak var definitionLabel: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

//        termLabel.text = card.term
//        definitionLabel.text = card.definition
        
        self.setup()
    }
    
    func setup()
    {
        topCardView.layer.cornerRadius = 8.0
        midCardView.layer.cornerRadius = 8.0
        botCardView.layer.cornerRadius = 8.0
        termView.layer.cornerRadius = 8.0
        defintionView.layer.cornerRadius = 8.0
    }
}
