//
//  Flashcard.swift
//  flashcards
//
//  Created by Julian Currie on 1/25/22.
//

import Foundation

struct Flashcard
{
    var term: String
    var definition: String
    
    mutating func returnSet() -> Array<Flashcard>
    {
        var setOfFlashcards: Array<Flashcard> = Array()
        for i in 1...10
        {
            let flashcard = Flashcard(term: "Term " + i.description, definition: "Definition" + i.description)
            setOfFlashcards.append(flashcard)
        }
        return setOfFlashcards
    }
}
