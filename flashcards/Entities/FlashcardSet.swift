//
//  FlashcardSet.swift
//  flashcards
//
//  Created by Julian Currie on 1/25/22.
//

import Foundation

struct FlashcardSet
{
    var term: String
    var flashcardSet: Array<Flashcard>
    
    mutating func returnFlashcardSet(setOfFlashcards: Array<Flashcard>) -> Array<FlashcardSet>
    {
        var setOfFlashcardSets: Array<FlashcardSet> = Array()
        for i in 1...10
        {
            let flashcardSet = FlashcardSet(term: "Title " + i.description, flashcardSet: flashcardSet)
            setOfFlashcardSets.append(flashcardSet)
        }
        return setOfFlashcardSets
    }
}
